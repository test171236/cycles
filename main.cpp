#include <iostream>

void PrintOddEvenNumbers(int N, bool isOdd)
{
    for (int i = isOdd; i <= N; i+=2)
        std::cout << i << std::endl;
}

int main()
{
    const int N = 10;

    std::cout << "Even numbers from 0 to " << N << ":\n";
    PrintOddEvenNumbers(N, false);

    std::cout << "Odd numbers from 0 to " << N << ":\n";
    PrintOddEvenNumbers(N, true);

    return 0;
}